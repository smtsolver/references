# References

# Books

- [Decision Procedures: An Algorithmic Point of View](https://www.amazon.com/Decision-Procedures-Algorithmic-Theoretical-Computer/dp/3662504960/ref=sr_1_1?s=books&ie=UTF8&qid=1499132161&sr=1-1&keywords=decision+procedures)
- [Stochastic Local Search : Foundations & Applications](https://www.amazon.com/Stochastic-Local-Search-Applications-Intelligence/dp/1558608729)
- [Handbook of Satisfiability: Volume 185 Frontiers in Artificial Intelligence and Applications](https://www.amazon.com/Handbook-Satisfiability-Artificial-Intelligence-Applications/dp/1586039296)

# SAT

- [Boolean Satisfiability](https://en.wikipedia.org/wiki/Boolean_satisfiability_problem)
- [Boolean Satisfiability: From Theoretical Hardness to Practical Success](http://cacm.acm.org/magazines/2009/8/34498-boolean-satisfiability-from-theoretical-hardness-to-practical-success/fulltext)
- [CNF Generator for Factoring Problems](http://www.cs.indiana.edu/cgi-pub/sabry/cnf.html)
- [CNF generator in DIMACS format. It produces common families of CNFs](https://github.com/MassimoLauria/cnfgen)
- [CBMC - Bounded Model checking for software](http://www.cprover.org/cbmc/)
- [Sudoku CNF generator](https://github.com/sergisiso/Sudoku-CNF-generator)
- [SAT solving - An alternative to brute force bitcoin mining](https://jheusser.github.io/2013/02/03/satcoin.html)
- [Understanding SAT by Implementing a Simple SAT Solver in Python](http://sahandsaba.com/understanding-sat-by-implementing-a-simple-sat-solver-in-python.html)
- [Solving the GCHQ christmas card with Python and pycosat](https://matthewearl.github.io/2015/12/10/gchq-xmas-card/)

# SMT

- [SMT-LIB](http://smtlib.cs.uiowa.edu/)
- [The Z3 Theorem Prover](https://github.com/Z3Prover/z3)
- [Rise4Fun z3](http://rise4fun.com/z3)
- [Sudoku solver](http://norvig.com/sudoku.html)
- [Hardest sudoku](http://www.telegraph.co.uk/news/science/science-news/9359579/Worlds-hardest-sudoku-can-you-crack-it.html)

# Symbolic and Concolic Execution

- [Symbolic Execution - Wikipedia](https://en.wikipedia.org/wiki/Symbolic_execution)
- [Concolic testing - Wikipedia](https://en.wikipedia.org/wiki/Concolic_testing)
- [DART: Directed Automated Random Testing](http://research.microsoft.com/en-us/um/people/pg/public_psfiles/talk-pldi2005.pdf)
- [PathCrawler: Automatic Generation of Path Tests by Combining Static and Dynamic Analysis](http://pathcrawler-online.com/pubs/edcc05.pdf)

# Translation

- [Intel manuals](https://www-ssl.intel.com/content/www/us/en/processors/architectures-software-developer-manuals.html)

# Program Synthesis

- [Program Synthesis, Explained](https://homes.cs.washington.edu/~bornholt/post/synthesis-for-architects.html)
- [Program Synthesis by Sketching](https://people.csail.mit.edu/asolar/papers/thesis.pdf)
- [ICFP - Calibrating Research in Program Synthesis Using 72,000 Hours of Programmer Time](http://research.microsoft.com/en-us/um/people/nswamy/papers/calibrating-program-synthesis.pdf)
- [Automated Synthesis of Symbolic Instruction Encodings from I/O Samples](http://research.microsoft.com/en-us/um/people/pg/public_psfiles/pldi2012.pdf)
- [Microsoft Program Synthesis using Examples SDK](https://microsoft.github.io/prose/)
- [Rolf Rolles - Program Synthesis in Reverse Engineering](https://www.youtube.com/watch?v=mFjSbxV_1vw)
- [Microsoft Flash Fill paper](http://research.microsoft.com/en-us/um/people/sumitg/pubs/popl11-synthesis.pdf)
- [Flash Fill - YouTube](https://www.youtube.com/watch?v=qHkgJFJR5cM)
- [SmartSynth: Synthesizing Smartphone Automation Scripts from Natural Language](http://research.microsoft.com/en-us/um/people/sumitg/pubs/mobisys13-abs.html)
- [SmartSynth demo - MP4](http://research.microsoft.com/en-us/um/people/sumitg/pubs/mobisys13.mp4)

# Intermediate Languages

- [REIL: A platform-independent intermediate representation of disassembled code for static code analysis](https://static.googleusercontent.com/media/www.zynamics.com/en//downloads/csw09.pdf)
- [The REIL language - Part I](http://blog.zynamics.com/2010/03/07/the-reil-language-part-i/)
- [OpenREIL](https://github.com/Cr4sh/openreil)

# Triton

- [Triton](http://triton.quarkslab.com/)
- [Triton documentation](http://triton.quarkslab.com/documentation/)

# Angr

- [Angr](https://github.com/angr/angr)
- [Angr documentation](https://github.com/angr/angr-doc)
- [Hackers turn to angr for automated exploit discovery and patching](http://www.theregister.co.uk/2016/03/13/hackers_angr_automated_exploit_discovery/?mt=1458000725437)

# Whitebox

- [SAGE: Whitebox Fuzzing for Security Testing](https://queue.acm.org/detail.cfm?id=2094081)
- [DynamoRIO](https://github.com/DynamoRIO/dynamorio)
- [Fuzzing and Symbolic Execution - Mayhem](http://blog.forallsecure.com/2016/02/09/unleashing-mayhem/)
- [Driller: Augmenting Fuzzing Through Selective Symbolic Execution](https://www.internetsociety.org/sites/default/files/blogs-media/driller-augmenting-fuzzing-through-selective-symbolic-execution.pdf)
